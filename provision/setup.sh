#!/bin/bash
sudo dpkg-reconfigure locales

echo "Provisioning "$1

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update && apt-get upgrade -y


sudo  apt-get update -y

debconf-set-selections <<< 'mysql-server mysql-server/root_password  password '$6
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password '$6

echo "Installing mysql"
sudo apt-get install -y mysql-server
sudo service mysql restart

echo 'Enabling MySQL remote access'
sed -i "s/bind-address\s*=\s*127.0.0.1/bind-address = 127.0.0.1/" /etc/mysql/my.cnf

echo "Installing php & nginx"
sudo apt-get install -q -y -f python-software-properties mysql-client nginx php5 php5-fpm php5-mysql php5-gd php5-memcached memcached

echo "Installing common php package"
sudo apt-get install -q -y -f php5-curl php5-intl php-pear php5-imagick php5-imap php5-mcrypt  php5-xcache php5-gd
sudo apt-get install -q -y -f apt-utils zip gzip bzip2 git npm nodejs nodejs-legacy dos2unix autoconf && npm install bower less gulp js-yaml -g grunt-cli

echo 'Creating srv structure'
mkdir -p /srv/vhosts
mkdir -p /srv/www
mkdir -p /srv/www/$2
mkdir -p /srv/www/collections.$2
mkdir -p /srv/log/nginx

echo 'Copying ssh config'
sudo cp -v /vagrant/provision/ssh_config /home/vagrant/.ssh/config
sudo chmod 600 /home/vagrant/.ssh/id_rsa

echo 'Copying php config'
sudo cp -v /vagrant/provision/php5.conf /etc/nginx/php5.conf

echo 'Copying vhosts file'
sudo cp -v /vagrant/provision/vhost.default /srv/vhosts/$2
sed -i 's/TITLE/'$3'/g' /srv/vhosts/$2
sed -i 's/HOSTNAME/'$2'/g' /srv/vhosts/$2

echo 'Copying ssh config'
sudo cp -v /vagrant/provision/ssh_config ~/.ssh/config

echo 'Installing composer'
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

echo 'Creating '$1' database'
mysql -uroot -p$6 -e "DROP DATABASE IF EXISTS "$5";"
mysql -uroot -p$6 -e "CREATE DATABASE "$5";"
mysql -uroot -p$6 -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'password';"

echo 'Copying nginx config'
mv /etc/nginx/nginx.conf  /etc/nginx/nginx.conf.bak
cp -r /vagrant/provision/nginx.conf  /etc/nginx/nginx.conf

composer create-project $7 /srv/www/$2 $8

echo 'Updating enviroment values'
sed -i 's/APP_URL=http:\/\/localhost/APP_URL=http:\/\/'$2'/g' /srv/www/$2/.env
sed -i 's/DB_DATABASE=homestead/DB_DATABASE='$5'/g' /srv/www/$2/.env
sed -i 's/DB_USERNAME=homestead/DB_USERNAME=root/g' /srv/www/$2/.env
sed -i 's/DB_PASSWORD=secret/DB_PASSWORD='$6'/g' /srv/www/$2/.env
sed -i 's/SESSION_DRIVER=file/SESSION_DRIVER=database/g' /srv/www/$2/.env

echo 'migrating laravel session table'
cd ..
cd /srv/www/$2
php artisan session:table
composer dump-autoload
php artisan migrate
cd ..

service nginx restart
